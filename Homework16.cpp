// Homework16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <ctime>
using namespace std;
int main()
{
	const int N = 10;
	int array[N];
	int rowSum;

	// Get current date
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	int remainderofDivision = now.tm_mday % N;
	
	for (int i = 0; i < N; i++)
	{
		rowSum = 0;
		for (int j = 0; j < N; j++)
		{
			array[i,j] = i + j;
			std::cout << array[i, j] << ' ';
			rowSum += array[i, j];
		}
		if (i == remainderofDivision)
		{
			cout << " Rows sum:" << rowSum;
		}
		cout << '\n';
		
	}
}

